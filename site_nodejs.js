var path = require('path'),
	express = require('express'),
	fs = require('fs'),
	app = express();

app.use('/life', function(req, res, next) {
	fs.readFile("./life.html", "utf8", function(err, data) {
		if (!err) {
			res.writeHead(200, {"Content-Type": "text/html"});
			res.write(data);
			res.end();
		}
	});
});

app.listen(80, function () {
  console.log('Сайт запущен на 80 порту!');
});